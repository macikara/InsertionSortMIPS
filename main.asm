#####################################################################
#                                                                   #
# Name:Mustafa Acıkaraoğlu                                          #
# KUSIS ID:29036                                                    #
#####################################################################

# This file serves as a template for creating 
# your MIPS assembly code for assignment 2

.eqv MAX_LEN_BYTES 400

#====================================================================
# Variable definitions
#====================================================================

.data
   arg_err_msg:       .asciiz   "Argument error"
   input_msg:         .asciiz   "Input integers: \n"
   sorted_list_show:  .asciiz   "Sorted List\n"
   no_duplicates:     .asciiz   "No Duplicates\n"
   sum_of_array:      .asciiz   "Sum of no duplicates array : "
   end_of_program:    .asciiz   "--PROGRAM ENDED--"
   dash:              .asciiz   "-"
   nval:              .asciiz   "n"
   bosluk:            .asciiz   " "
   sifir:	      .asciiz   "0"
   dokuz:	      .asciiz   "9"
   newline:           .asciiz   "\n"
   input: 	      .asciiz   ""
   input_data:        .space    MAX_LEN_BYTES     #Define length of input list

#  You can define other data as per your need. 


#==================================================================== 
# Program start
#====================================================================

.text
.globl main

main:
   #
   # Main program entry
   #
   #
   # Main body
   # 
   
   # Check for command line arguments count and validity
   la $a0, input#get the input string, so we can check the validity
   la $a1, input_data#read this many bytes
   li $v0,8
   syscall
   
   move $s0,$a0#move the string into $s0 to check it on another label
   
   #add $a0,$zero,$zero#no need for this
   
   j validate_args#jump to validate the entered args

Data_Input:
   ########################################################################
   beq $s1, $zero, Arg_Err#no point of sorting an array that its size is 0#
   addi $t7,$zero,1                                                       #
   beq $s1,$t7,Arg_Err#no point of sorting an array that its size is 1    #
   ########################################################################
   
   # Get integers from user as per value of n
   la $a0,input_msg#call to enter integers
   li $v0,4
   syscall
  ####################################################
   addi $t0 , $zero ,0
   ##################  YOUR CODE  ####################
   sll $a0,$s1,2#initialize an int array with the size of $s1
   li $v0,9
   syscall
   move $t1,$v0#array pointer -- address
   move $t2 ,$t1# move it onto another register to keep the starting point
   Get_Integer:
      li $v0,5
      syscall
      sw $v0, 0($t2)
      addi $t2,$t2,4
      addi $t0 ,$t0,1
   bne $t0,$s1,Get_Integer
   
   j sort_data

 
# Insertion sort begins here
sort_data:
   ##################  YOUR CODE  ####################
   addi $t0,$zero,0
   addi $t0,$t0,1 #i counter
   move $t2,$t1 #i addr at $t2(original at $t1)
   Inner_Loop_First:
      move $t4,$t2 #j addr move the current i addr counter as j addr counter
      addi $t4,$t4,4
      Inner_Loop_Second:
         lw $t6, 0($t4)
         lw $t7, -4($t4)
         bgt $t6,$t7, Dont_Swap#if left value is bigger than right value, swap, else do nothing
         sw $t7,0($t4)
         sw $t6, -4($t4)
         Dont_Swap:
         addi $t4,$t4,-4
      bne $t4,$t1,Inner_Loop_Second#do it until the saved address reaches to 0
      addi $t2,$t2,4
      addi $t0,$t0,1
   bne $t0,$s1,Inner_Loop_First
   
   j print_w_dup#jump to print the first array
   
   
remove_duplicates:
   ##################  YOUR CODE  ####################
   sll $a0,$s1,2#size is on s1 (first array)
   li $v0,9#create array on heap
   syscall
   move $s2, $v0#move the created arrays starting address to $s2 (will always be $s2)
   addi $t0, $zero,1
   move $t2,$s2
   lw $t7,0($t1)
   sw $t7, 0($t2)
   addi $t2,$t2,4
   addi $t4,$zero,0
   Frr_lpp:
      addi $t1,$t1,4
      lw $t6, 0($t1)
      lw $t7,-4($t1)
      beq $t6,$t7,Dont_Save
      sw $t6,0($t2)
      addi $t2,$t2,4
      addi $t4,$t4,1
      Dont_Save:#jump to this to avoid counting the second array counter (and not counting the initial counter)and be stuck at infinite loop
         addi $t0,$t0,1
   bne $s1,$t0,Frr_lpp
   
   sll $a0,$t4,2
   li $v0,9
   syscall
   move $s4,$v0
   move $t7,$s4#sorted array
   move $t2,$s2
   addi $t0,$zero,0
   addi $t4,$t4,1
   Copy_To_New_Array:
      lw $t6 ,0($t2)#if the right value is not equal to the left value than the right element is unique and can be added to the new array
      sw $t6,0($t7) #(since the array is sorted)
      addi $t0,$t0,1
      addi $t7,$t7,4
      addi $t2,$t2,4
   bne $t0,$t4,Copy_To_New_Array
   j print_wo_dup
   
# Print sorted list with and without duplicates
print_w_dup:
   ##################  YOUR CODE  ####################
   la $a0,sorted_list_show
   li $v0,4
   syscall
   
   addi $t0,$zero,0
   move $t2,$t1
   Inner_For_Foor:
      lw $a0,0($t2)
      li $v0,1
      syscall
      la $a0, bosluk
      li $v0,4
      syscall
      addi $t2,$t2,4
      addi $t0,$t0,1
   bne $t0,$s1,Inner_For_Foor
   
   la $a0 , newline
   li $v0,4
   syscall
   
   j remove_duplicates
   
print_wo_dup:
   ##################  YOUR CODE  ####################
   la $a0,no_duplicates
   li $v0,4
   syscall
   
   addi $t0,$zero,0
   move $t5,$s4
   Inner_For_Fooor:
      lw $a0,0($t5)
      li $v0,1
      syscall
      la $a0, bosluk
      li $v0,4
      syscall
      addi $t5,$t5,4
      addi $t0,$t0,1
   bne $t0,$t4,Inner_For_Fooor
   
   la $a0,newline
   li $v0,4
   syscall
   
   la $a0,sum_of_array
   syscall
   
# Perform reduction
##################  YOUR CODE  ####################
   addi $t0,$zero,0#counter
   move $t7, $s4
   addi $t3,$zero, 0#sum
   Reduction:
      addi $t0,$t0,1
      lw $t2,0($t7)
      add $t3,$t3,$t2#sum all the elements of third array into $t3
      addi $t7,$t7,4
   bne $t0,$t4,Reduction
   
   
# Print sum
   li  $v0, 1
   addi $a0, $t3, 0      # $t3 contains the sum  
   syscall
   
   la $a0, newline
   li $v0,4
   syscall
   
   la $a0,end_of_program
   syscall
  
  j Exit
   
validate_args:

   lb $t0, 0($s0)
   la $t1, dash
   lb $t1, 0($t1)
   bne $t1,$t0 ,Arg_Err
   
   lb $t5, 1($s0)
   la $t1, nval
   lb $t1, 0($t1)
   bne $t5,$t1,Arg_Err
   
   lb $t5,2($s0)
   la $t1,bosluk
   lb $t1, 0($t1)
   bne $t5,$t1,Arg_Err
   
   lb $t5 ,3($s0)
   la $t1, sifir
   lb $t1, 0($t1)
   la $t2, dokuz
   lb $t2, 0($t2)
   blt $t5, $t1, Arg_Err#check if the ASCII symbol is between the symbol "0" and the symbol "9"#
   bgt $t5,$t2,Arg_Err##
   addi $t5 , $t5 ,-48#convert from ASCII to INT(since the ASCII the symbol "0" is 48)
   move $s1,$t5#if the entered integer is between 0 and 9(has 1 digit) write its value to $s1 to not forget it
      
   lb $t4, 4($s0)
   la $t6, newline
   lb $t6 0($t6)   
   beq $t4,$t6,Data_Input#if there is only one-digit integer
   blt $t4,$t1,Arg_Err
   bgt $t4,$t2,Arg_Err
   
   addi $t6,$zero,10
   addi $t4 , $t4 ,-48#convert from ASCII to INT
   mult $t5,$t6#if the entered int is between 10-99 (has 2 digits) multiply the first digit with 10 and add it to the second to recreate the entered integer
   mflo $t5#move the low address to #t5
   add $s1,$t4,$t5#save the result int into $s1(size is at $s1, always !)
   ############################################important to note that this assembly code will only work for numbers 0-99
   #(i didnt do it for 3-digit or more digit integers)
   j Data_Input
   
Arg_Err:
   # Error message when no input arguments specified
   # or when argument format is not valid
   la $a0, arg_err_msg
   li $v0, 4
   syscall
   j Exit

Exit:   
   # Jump to this label at the end of the program
   li $v0, 10
   syscall
