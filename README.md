just run the code on MARS MIPS simulator

1. After assembling, run the code
2. the first 3 bytes of the input string must be "-n " ,if not there will be a arg error and program will exit
3. the 4.th and 5.th byte should be integers, if not again arg error
4. if more than 5 bytes of characters are given the program will only do it for the first 5 bytes.

Examples:
    -n 6  #Size of the array which will be sorted is 6
    -n 60 #Size of the array which will be sorted is 60(the program will ask the user 60 times to enter an integer)
    -n 600 == -n 60 #Since if the string size is bigger than 5 the remaining char sequence will be ignored
                    (This ensures that the N value can only be 99 maximum)
    -a 6  #arg error
    -n abc#arg error
    -n 10 #program will ask the user for 10 integers, sort the given integers, remove duplicates and print the sum on the console.

All the desired tasks are implemented in this project. (Every part of the project works, there is no malfunction or missing part.)
